using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CBattleRoom : MonoBehaviour {
	
	public int modo=0;//0 un juegador, 1 dos jugadores, 2 tres jugadores,3 cuatro jugadores
	byte cantidadPlayers=0;
	int contador=0;
	int disponibles=45;
	int player0=1;
	int player1=1;
	int player2=1;
	int player3=1;
	bool checkplayer0= false;
	bool checkplayer1= false;
	bool checkplayer2= false;
	bool checkplayer3= false;
	public static readonly int COL_COUNT =7;
	List<CPlayer> players;
	List<short> board;
	List<short> table_board;

	List<Texture> img_players;
	Texture background;
	//Texture blank_image;
	Texture game_board;
	GUISkin blank_skin;
	
	//Texture graycell;
	Texture focus_cell;
	Texture button_playagain;
	
	List<short> available_attack_cells;
	byte current_player_index;
	byte step;
	
	void Awake()
	{
		this.table_board = new List<short>();
		this.available_attack_cells = new List<short>();
		//this.graycell = Resources.Load("images/graycell") as Texture;
		this.focus_cell = Resources.Load("images/border") as Texture;
		
		this.blank_skin = Resources.Load("blank_skin") as GUISkin;
		//this.blank_image = Resources.Load("images/blank") as Texture;
		this.game_board = Resources.Load("images/gameboard") as Texture;
		this.background = Resources.Load("images/gameboard_bg") as Texture;
		this.img_players = new List<Texture>();
		this.img_players.Add(Resources.Load("images/red 1") as Texture);//aqui se asigna la textura del jugador
		this.img_players.Add(Resources.Load("images/blue 1") as Texture);//aqui se asigna la texutra del enemigo
		this.img_players.Add(Resources.Load("images/red") as Texture);//aqui se asigna la textura del jugador
		this.img_players.Add(Resources.Load("images/blue") as Texture);//aqui se asigna la texutra del enemigo
		
		this.button_playagain = Resources.Load("images/playagain") as Texture;
		
		this.players = new List<CPlayer>();
		//----------------------------------
		if (modo == 0 || modo==1) 
		{
			cantidadPlayers=2;
			checkplayer2=true;
			checkplayer3=true;
		}
		if (modo == 2 || modo==3) 
		{
			cantidadPlayers=4;
		}
		//---------------------------------------cantidad de jugadores
		for (byte i=0; i<cantidadPlayers; ++i)
		{
			GameObject obj = new GameObject(string.Format("player{0}", i));
			CPlayer player = obj.AddComponent<CPlayer>();
			player.initialize(i);
			this.players.Add(player);
		}

		this.board = new List<short>();
		
		reset ();
	}

//-------------------------------------------------------------------------------------
	void  Start (){
		InvokeRepeating("animacion",1,1);
	}
//-------------------------------------------------------------------------------------

	void reset()
	{
				this.players.ForEach (obj => obj.clear ());
				//-------------------------------------------------------posicion jugadores
				if (modo == 0) 
				{
				this.players [0].add (42);// 6  42  0 48
				this.players [1].add (0);
				this.players [0].add (6);
				this.players [1].add (48);
				}
				if (modo == 1) 
				{
					this.players [0].add (42);
					this.players [1].add (0);
					this.players [0].add (6);
					this.players [1].add (48);
				}
				if (modo == 2) 
				{
					this.players [0].add (42);
					this.players [1].add (0);
					this.players [2].add (6);
					this.players [3].add (48);
				}
				if (modo == 3) 
				{
					this.players [0].add (42);
					this.players [1].add (0);
					this.players [2].add (6);
					this.players [3].add (48);
				}
				//---------------------------------------------------------tipo de jugadores
				if (modo == 0) 
				{
				this.players [1].change_to_AI1();//turno enemigo
				}
				if (modo == 1) 
				{
			//this.players [1].change_to_AI1();//turno enemigo
			this.players [1].change_to_player2();//turno enemigo
				}
				if (modo == 2) 
				{
				this.players [1].change_to_player2();
				this.players [2].change_to_player3();
				this.players [3].change_to_AI1();
				}
				if (modo == 3) 
				{
					this.players [1].change_to_player2();
					this.players [2].change_to_player3();
					this.players [3].change_to_player4();
				}
				//------------------------------------------------------
				//this.players [1].change_to_player2();//turno enemigo
				//this.players [2].change_to_player3();//turno enemigo
				//this.players [3].change_to_player4();//turno enemigo
				this.board.Clear ();
				this.table_board.Clear ();
				for (int i=0; i<COL_COUNT * COL_COUNT; ++i) {
						this.board.Add (short.MaxValue);
						this.table_board.Add ((short)i);
				}
		
				this.players.ForEach (obj => {
						obj.cell_indexes.ForEach (cell => {
								this.board [cell] = obj.player_index;
						});
				});
		
				this.current_player_index = 0;//seleccion
				this.step = 0;//turno jugador 1
	}
	
	float ratio = 1.0f;
	void OnGUI()
	{
		ratio = Screen.width / 800.0f;
		
		GUI.skin = this.blank_skin;
		draw_board();
		
		if (GUI.Button(new Rect(10,10,80 * ratio, 80 * ratio), this.button_playagain))
		{
			StopAllCoroutines();
			reset();
		}
	}
	
	void draw_board()
	{
		float scaled_height = 480.0f * ratio;
		float gap_height = Screen.height - scaled_height;
		
		float outline_left = 0;
		float outline_top = gap_height * 0.5f;
		float outline_width = Screen.width;
		float outline_height = scaled_height;
		
		float hor_center = outline_width * 0.5f;
		float ver_center = outline_height * 0.5f;

		GUI.BeginGroup(new Rect(0, 0, outline_width, Screen.height));
		
		// Dibuja el fondo de toda la pantalla.
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), this.background);
		
		// Dibujar una tabla (alineación: centro).
		GUI.DrawTexture(new Rect(0, outline_top, outline_width, outline_height), this.game_board);

		int width = (int)(60 * ratio);
		int celloutline_width = width * CBattleRoom.COL_COUNT;
		float half_celloutline_width = celloutline_width*0.5f;
		
		GUI.BeginGroup(new Rect(hor_center-half_celloutline_width, 
			ver_center-half_celloutline_width + outline_top, celloutline_width, celloutline_width));
		
		List<int> current_turn = new List<int>();
		short index = 0;
		for (int row=0; row<CBattleRoom.COL_COUNT; ++row)
		{
			int gap_y = 0;//(row * 1);
			for (int col=0; col<CBattleRoom.COL_COUNT; ++col)
			{
				int gap_x = 0;//(col * 1);
				
				Rect cell_rect = new Rect(col * width + gap_x, row * width + gap_y, width, width);
				if (GUI.Button(cell_rect, ""))
				{
					on_click(index);//controles para las celdas
				}
				
				if (this.board[index] != short.MaxValue)
				{
					int player_index = this.board[index];
					GUI.DrawTexture(cell_rect, this.img_players[player_index]);
					
					if (this.current_player_index == player_index)
					{
						GUI.DrawTexture(cell_rect, this.focus_cell);
					}
				}
								
				if (this.available_attack_cells.Contains(index))
				{
					GUI.DrawTexture(cell_rect, this.focus_cell);
				}
				
				++index;
			}
		}
		GUI.EndGroup();
		GUI.EndGroup();
	}

	short selected_cell = short.MaxValue;
	void on_click(short cell)
	{
		//Debug.Log(cell);
		
		switch(this.step)
		{
		case 0:
			if (validate_begin_cell(cell))
			{
				this.selected_cell = cell;
				//Debug.Log("go to step2");
				this.step = 1;//jugador 2
				//print(this.step);
				refresh_available_cells(this.selected_cell);
			}
			else {
				print ("no hay mas movimientos");
				this.step = 0;
				refresh_available_cells(this.selected_cell);
			}
			break;
			
		case 1:
		{
			// Cuando tocaste tu celular nuevo.
			if (this.players[this.current_player_index].cell_indexes.Exists(obj => obj == cell))
			{
				this.selected_cell = cell;
				refresh_available_cells(this.selected_cell);
				break;
			}
			// No se puede tocar la celda de otro jugador.
			foreach(CPlayer player in this.players)
			{
				if (player.cell_indexes.Exists(obj => obj == cell))
				{
					this.step = 2;
					//refresh_available_cells(this.selected_cell);
					return;
				}
			}
			
			//jugador 3
			//print(this.step);
			StartCoroutine(on_selected_cell_to_attack(cell));
		}
		break;
			
		case 2:
		{
			// Playin AI now.
			//print ("cambio de turno");
			if(this.current_player_index ==0)
			{
				player0=0;
			}
			if(this.current_player_index ==1)
			{
				player1=0;
			}
			if(this.current_player_index ==2)
			{
				player2=0;
			}
			if(this.current_player_index ==3)
			{
				player3=0;
			}
			print ("CAMBIO DE TURNO");
			if(modo==0 || modo==1)
			{
			//this.step=0;
				phase_end();
			}
			else
			{
			phase_end();
			}
			break;
		}
		/*case 3:
		// Playin AI now.
		print ("cambio de turno");
		break;*/
	}
	}
	
	IEnumerator on_selected_cell_to_attack(short cell)
	{
		byte distance = CHelper.howfar_from_clicked_cell(this.selected_cell, cell);
		if (distance == 1)
		{
			// copiar a la celda
			if(this.current_player_index==0)
			{
				player0= player0+1;
			}
			if(this.current_player_index==1)
			{
				player1= player1+1;
			}
			if(this.current_player_index==2)
			{
				player2= player2+1;
			}
			if(this.current_player_index==3)
			{
				player3= player3+1;
			}
			disponibles=disponibles-1;
			//print ("disponibles:"+disponibles);
			//print ("Zorro:"+player0);
			//print ("Gato:"+player1);
			//print ("Rojo:"+player2);
			//print ("Azul:"+player3);
			yield return StartCoroutine(reproduce(cell));
			print ("TE TOCA A VOS");
			phase_end();
		}
		else if (distance == 2)
		{
			// mover
			this.board[this.selected_cell] = short.MaxValue;
			this.players[this.current_player_index].remove(this.selected_cell);
			yield return StartCoroutine(reproduce(cell));
			print ("YA MOVI");
			phase_end();
		}
		
		yield return 0;
	}
	
	void game_over()
	{
		Debug.Log("GameOver!");
		if (player0 <= 0 && checkplayer0==false) 
		{
			/*if(player1 > 0 && (player2 > 0 || player3 > 0))
			{*/
				this.players[0].clear();
				checkplayer0=true;
				phase_end();
			/*}
			else
			{
				if(player2 > 0 && (player1 > 0 || player3 > 0))
				{
					this.players[0].clear();
					checkplayer0=true;
				}
			}*/
		}
		if (player1 <= 0 && checkplayer1==false) 
		{
			/*if(player0 > 0 && (player2 > 0 || player3 > 0))
			{*/
				this.players[1].clear();
				checkplayer1=true;
				phase_end();
			/*}
			else
			{
				if(player2 > 0 && (player0 > 0 || player3 > 0))
				{
					this.players[1].clear();
					checkplayer1=true;
				}
			}*/
		}
		if (player2 <= 0 && checkplayer2==false) 
		{
			/*if(player1 > 0 && (player0 > 0 || player3 > 0))
			{*/
				this.players[2].clear();
				checkplayer2=true;
				phase_end();
			/*}
			else
			{
				if(player0 > 0 && (player1 > 0 || player3 > 0))
				{
					this.players[2].clear();
					checkplayer2=true;	
				}
			}*/
		}
		if (player3 <= 0 && checkplayer3==false) 
		{
			/*if(player1 > 0 && (player2 > 0 || player0 > 0))
			{*/
				this.players[3].clear();
				checkplayer3=true;
				phase_end();
			/*}
			else
			{
				if(player2 > 0 && (player1 > 0 || player0 > 0))
				{
					this.players[3].clear();
					checkplayer3=true;
				}
			}*/
		}
		//-----------------------------------------------------------------------
		//print (checkplayer0);
		//print (checkplayer1);
		//print (checkplayer2);
		//print (checkplayer3);
		if (checkplayer0 == true) 
		{
						if (checkplayer1 == true && (checkplayer2 == true || checkplayer3 == true)) {
								win ();
						}
						if(checkplayer2==true && (checkplayer1==true || checkplayer3== true))		
						{
							win();
						}
		} 
		else 
		{
			if (checkplayer1 == true) 
			{
				if(checkplayer0==true && (checkplayer2==true || checkplayer3== true))		
				{
					win();
				}
				if(checkplayer2==true && (checkplayer0==true || checkplayer3== true))		
				{
					win();
				}
			}

			else
			{
				if (checkplayer2 == true) 
				{
					if(checkplayer1==true && (checkplayer0==true || checkplayer3== true))		
					{
						win();
					}
					if(checkplayer0==true && (checkplayer1==true || checkplayer3== true))		
					{
						win();
					}
				}

				else
				{
					if (checkplayer3 == true) 
					{
						if(checkplayer1==true && (checkplayer2==true || checkplayer0== true))		
						{
							win();
						}
						if(checkplayer2==true && (checkplayer0==true || checkplayer1== true))		
						{
							win();
						}
					}
				}
			}
		}
		if (disponibles <= 0) 
		{
			win();
		}
		//--------------------------------------------------------------------------
	}
	void win()
	{
		//----------------------------------------------------------------
		if (player0 > player1 && player0 > player2 && player0 > player3) 
		{
			Ganar.gano=0;
			print ("ganador:"+player0);
			print ("otros:"+player1);
			print ("otros:"+player2);
			print ("otros:"+player3);
		}
		if (player1 > player0 && player1 > player2 && player1 > player3) 
		{
			Ganar.gano=1;	
			print ("ganador:"+player1);
			print ("otros:"+player0);
			print ("otros:"+player2);
			print ("otros:"+player3);

		}
		if (player2 > player1 && player2 > player0 && player2 > player3) 
		{
			Ganar.gano=2;	
			print ("ganador:"+player2);
			print ("otros:"+player1);
			print ("otros:"+player0);
			print ("otros:"+player3);
		}
		if (player3 > player1 && player3 > player2 && player3 > player0) 
		{
			Ganar.gano=3;	
			print ("ganador:"+player3);
			print ("otros:"+player1);
			print ("otros:"+player2);
			print ("otros:"+player0);
		}
		//------------------------------------------------------------------
		Application.LoadLevel ("Perder");
	}
	void phase_end()
	{
		CPlayer victim_player = this.players[this.current_player_index];
		//print (this.current_player_index);
		//cambio de jugador
		if (modo == 0 || modo == 1) 
		{
			print (this.current_player_index);
		//-----------------------------------------------------------------------
			if (this.current_player_index == 0) {
					this.current_player_index = 1;
			} else {
				if (this.current_player_index == 1) {
						this.current_player_index = 0;
				}
			}
		//-----------------------------------------------------------------------
		}
		if (modo == 2 || modo == 3) 
		{
				print (this.current_player_index);
		//--------------------------------------------------------------------
						if (this.current_player_index == 0) 
						{
								if (checkplayer1 == false) 
								{
										this.current_player_index = 1;
								} 
								else 
								{
										if (checkplayer2 == false) 
										{
												this.current_player_index = 2;
										} 
										else 
										{
												this.current_player_index = 3;
										}
								}

						} 
						else 
						{
								if (this.current_player_index == 1) 
								{
										if (checkplayer2 == false) 
										{
												this.current_player_index = 2;
										} 
										else 
										{
												if (checkplayer3 == false) 
												{
														this.current_player_index = 3;
												} 
												else 
												{
														this.current_player_index = 0;
												}
										}
				
								} 
								else 
								{
										if (this.current_player_index == 2) 
										{
												if (checkplayer3 == false) 
												{
														this.current_player_index = 3;
												} else 
												{
														if (checkplayer0 == false) 
														{
																this.current_player_index = 0;
														} else 
														{
																this.current_player_index = 1;
														}
												}
					
										} 
										else 
										{
												if (this.current_player_index == 3) 
												{
														if (checkplayer0 == false) 
														{
																this.current_player_index = 0;
														} 
														else 
														{
																if (checkplayer1 == false) 
																{
																		this.current_player_index = 1;
																} 
																else 
																{
																		this.current_player_index = 2;
																}
														}
						
												}
										}
								}
						}
				//}
			//----------------------------------------------------------------------
			
				//this.current_player_index = 0;
		}
		
		
		if (!CHelper.can_play_more(this.table_board, this.players, this.current_player_index) || disponibles<=0)
		{
			print (disponibles);
			game_over();
			return;
		}
		
		CPlayer attacker_player = this.players[this.current_player_index];
		if (attacker_player.state == PLAYER_STATE.AI1)
		{
			this.step = 0;
			//this.step = 2;
			print(this.step);
			StartCoroutine(play_agent(attacker_player, victim_player));
		}
		this.available_attack_cells.Clear();
	}
	
	IEnumerator play_agent(CPlayer attacker_player, CPlayer victim_player)
	{
		CellInfo choice = attacker_player.run_agent(this.table_board, this.players, victim_player.cell_indexes);
		yield return new WaitForSeconds(0.5f);

		//Debug.Log(string.Format("{0} -> {1} = {2}", choice.from_cell, choice.to_cell, choice.score)); HABILITAR
		this.selected_cell = choice.from_cell;
		StartCoroutine(on_selected_cell_to_attack(choice.to_cell));
	}
	
	void refresh_available_cells(short cell)
	{
		this.available_attack_cells = CHelper.find_available_cells(cell, this.table_board, this.players);
	}
	
	void clear_available_attacking_cells()
	{
		this.available_attack_cells.Clear();
	}
	
	//IEnumerator moving()
	//{
	//}
	
	IEnumerator reproduce(short cell)
	{
		CPlayer current_player = this.players[this.current_player_index];
		//CPlayer other_player = this.players.Find(obj => obj.player_index != this.current_player_index);
		CPlayer other_player1= this.players.Find(obj => obj.player_index != this.current_player_index);
		CPlayer other_player2= this.players.Find(obj => obj.player_index != this.current_player_index && other_player1.player_index != obj.player_index );
		CPlayer other_player3= this.players.Find(obj => obj.player_index != this.current_player_index && other_player1.player_index != obj.player_index && other_player2.player_index != obj.player_index);
		//print ("soy comible:"+other_player1);	
		//print ("soy comible:"+other_player2);
		//print ("soy comible:"+other_player3);
		clear_available_attacking_cells();
		yield return new WaitForSeconds(0.5f);
		
		this.board[cell] = current_player.player_index;
		current_player.add(cell);

		yield return new WaitForSeconds(0.5f);
		
		// comer
		//--------------------------------------------------------------------------------------------------------------
		List<short> neighbors1 = CHelper.find_neighbor_cells(cell, other_player1.cell_indexes, 1/*Espacio para comer*/);
		foreach (short obj in neighbors1)
		{
			//---------------------------------------------------
			if(this.current_player_index==0)
			{
				//print ("zorro vamosss");
				player0= player0+1;
			}
			if(this.current_player_index==1)
			{
				//print ("gato vamosss");
				player1= player1+1;
			}
			if(this.current_player_index==2)
			{
				//print ("rojo vamosss");
				player2= player2+1;
			}
			if(this.current_player_index==3)
			{
				//print ("azul vamosss");
				player3= player3+1;
			}
			//-----------------------------------------
			if(other_player1.player_index==0)
			{
				//print ("zorro muere");
				player0= player0-1;
			}
			if(other_player1.player_index==1)
			{
				//print ("gato muere");
				player1= player1-1;
			}
			if(other_player1.player_index==2)
			{
				//print ("rojo muere");
				player2= player2-1;
			}
			if(other_player1.player_index==3)
			{
				//print ("azul muere");
				player3= player3-1;
			}
			//-------------------------------------------------------
			this.board[obj] = current_player.player_index;
			current_player.add(obj);
			
			other_player1.remove(obj);
			//print ("soy:"+current_player);
			//print ("me comi a:"+other_player1);
			//phase_end();
			yield return new WaitForSeconds(0.2f);
		}
		//-------------------------------------------------------------------------------------------------------------
		if (modo == 2 || modo == 3) 
		{
			//--------------------------------------------------------------------------------------------------------------
			List<short> neighbors2 = CHelper.find_neighbor_cells(cell, other_player2.cell_indexes, 1/*Espacio para comer*/);
			foreach (short obj in neighbors2)
			{
				//----------------------------------------------
				if(this.current_player_index==0)
				{
					print ("zorro vamosss");
					player0= player0+1;
				}
				if(this.current_player_index==1)
				{
					print ("gato vamosss");
					player1= player1+1;
				}
				if(this.current_player_index==2)
				{
					print ("rojo vamosss");
					player2= player2+1;
				}
				if(this.current_player_index==3)
				{
					print ("azul vamosss");
					player3= player3+1;
				}
				if(other_player2.player_index==0)
				{
					print ("zorro muere");
					player0= player0-1;
				}
				if(other_player2.player_index==1)
				{
					print ("gato muere");
					player1= player1-1;
				}
				if(other_player2.player_index==2)
				{
					print ("rojo muere");
					player2= player2-1;
				}
				if(other_player2.player_index==3)
				{
					print ("azul muere");
					player3= player3-1;
				}
				//----------------------------------------------
				this.board[obj] = current_player.player_index;
				current_player.add(obj);
				
				other_player2.remove(obj);
				//print ("soy:"+current_player);
				//print ("me comi a:"+other_player2);
				yield return new WaitForSeconds(0.2f);
			}
			//-------------------------------------------------------------------------------------------------------------
			//--------------------------------------------------------------------------------------------------------------
			List<short> neighbors3 = CHelper.find_neighbor_cells(cell, other_player3.cell_indexes, 1/*Espacio para comer*/);
			foreach (short obj in neighbors3)
			{
				//-------------------------------------------------
				if(this.current_player_index==0)
				{
					print ("zorro vamosss");
					player0= player0+1;
				}
				if(this.current_player_index==1)
				{
					print ("gato vamosss");
					player1= player1+1;
				}
				if(this.current_player_index==2)
				{
					print ("rojo vamosss");
					player2= player2+1;
				}
				if(this.current_player_index==3)
				{
					print ("azul vamosss");
					player3= player3+1;
				}
				if(other_player3.player_index==0)
				{
					print ("zorro muere");
					player0= player0-1;
				}
				if(other_player3.player_index==1)
				{
					print ("gato muere");
					player1= player1-1;
				}
				if(other_player3.player_index==2)
				{
					print ("rojo muere");
					player2= player2-1;
				}
				if(other_player3.player_index==3)
				{
					print ("azul muere");
					player3= player3-1;
				}
				//-------------------------------------------------
				this.board[obj] = current_player.player_index;
				current_player.add(obj);
				
				other_player3.remove(obj);
				//print ("soy:"+current_player);
				//print ("me comi a:"+other_player3);
				yield return new WaitForSeconds(0.2f);
			}
		}
		//-------------------------------------------------------------------------------------------------------------
	}
	
	bool validate_begin_cell(short cell)
	{
		return this.players[this.current_player_index].cell_indexes.Exists(obj => obj == cell);
	}

	void  animacion (){
		//print("hola");
		if (contador < 4) 
		{
		contador++;
		}
		if (contador == 1)
		{
		this.img_players [0] = (Resources.Load ("images/red 1") as Texture);
		this.img_players [1] = (Resources.Load ("images/blue 1") as Texture);
		this.img_players [2] = (Resources.Load ("images/red") as Texture);
		this.img_players [3] = (Resources.Load ("images/blue") as Texture);
		}
		if (contador == 2) 
		{
		this.img_players [0] = (Resources.Load ("images/red 2") as Texture);
		this.img_players [1] = (Resources.Load ("images/blue 2") as Texture);
		this.img_players [2] = (Resources.Load ("images/red") as Texture);
		this.img_players [3] = (Resources.Load ("images/blue") as Texture);
		}
		if (contador == 3) 
		{
		this.img_players [0] = (Resources.Load ("images/red 3") as Texture);
		this.img_players [1] = (Resources.Load ("images/blue 3") as Texture);
		this.img_players [2] = (Resources.Load ("images/red") as Texture);
		this.img_players [3] = (Resources.Load ("images/blue") as Texture);
		}
		if (contador == 4) 
		{
		this.img_players [0] = (Resources.Load ("images/red 4") as Texture);
		this.img_players [1] = (Resources.Load ("images/blue 4") as Texture);
		this.img_players [2] = (Resources.Load ("images/red") as Texture);
		this.img_players [3] = (Resources.Load ("images/blue") as Texture);
		}
		if (contador == 4) 
		{
		contador=0;
		}
	}
}
