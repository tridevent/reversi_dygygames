using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CBattleRoom2 : MonoBehaviour {
	
	Font Font_nombre;
	AudioClip sonido_come;
	AudioClip sonido_come2;
	AudioClip sonido_next;
	AudioClip sonido_again;
	Texture cuadros;
	//--------------------------------------
	int contador=0;
	int disponibles=45;
	int player0=1;
	int player1=1;
	int player2=1;
	int player3=1;
	bool checkplayer0= false;
	bool checkplayer1= false;
	bool checkplayer2= false;
	bool checkplayer3= false;
	//---------------------------------------
	//private float yE=0;
	private float xP;
	private float xP2;
	private float yP;
	private float yP2;
	//------------------------------------------
	//private float yE=0;
	private float TxP;
	private float TxP2;
	private float TyP;
	private float TyP2;
	//------------------------------------------
	private float yE;
	private float BxP;
	private float BxP2;
	private float BxP3;
	private float ByP;
	private float ByP2;
	private float yE2;
	private float xP3;
	//------------------------------------------
	//byte aux=0;
	public static readonly int COL_COUNT =7;
	List<CPlayer> players;
	List<short> board;
	List<short> table_board;
	
	List<Texture> img_players;
	//Texture blank_image;
	//Texture game_board;
	GUISkin blank_skin;
	GUISkin blank_skin1;
	
	//Texture graycell;
	Texture focus_cell;
	Texture button_playagain;
	Texture button_next;
	Texture button_nextDes;
	//-----------------------
	Texture button_salir;
	Texture button_salirDes;
	Texture button_aceptarDes;
	Texture button_rechazarDes;
	bool apertura=true;
	//-----------------------
	
	List<short> available_attack_cells;
	byte current_player_index;
	byte step;
	
	void Awake()
	{
		this.table_board = new List<short>();
		this.available_attack_cells = new List<short>();
		//this.graycell = Resources.Load("images/graycell") as Texture;
		this.focus_cell = Resources.Load("images/border") as Texture;
		
		this.blank_skin = Resources.Load("blank_skin") as GUISkin;
		this.blank_skin1 = Resources.Load("blank_skin 1") as GUISkin;
		this.img_players = new List<Texture>();
		this.img_players.Add(Resources.Load("images/Personajes/Perro/Perro_02") as Texture);//aqui se asigna la textura del jugador
		this.img_players.Add(Resources.Load("images/Personajes/Gato/Gato_02") as Texture);//aqui se asigna la texutra del enemigo
		this.img_players.Add(Resources.Load("images/Personajes/Raton/Raton_02") as Texture);//aqui se asigna la textura del jugador
		this.img_players.Add(Resources.Load("images/Personajes/Cerdo/Cerdo_02") as Texture);//aqui se asigna la texutra del enemigo
		
		this.button_playagain = Resources.Load("images/Iconos/Reiniciar_Nivel_Des") as Texture;
		this.button_next = Resources.Load("images/Iconos/Pasar_Turno_Act") as Texture;
		this.button_nextDes = Resources.Load("images/Iconos/Pasar_Turno_Des") as Texture;
		this.sonido_next= Resources.Load("sonidos/Saltear") as AudioClip;
		this.sonido_again= Resources.Load("sonidos/Reiniciar") as AudioClip;
		//----------------------------------------------------------------------------------
		this.button_salir = Resources.Load("images/Iconos/Salir_Act") as Texture;
		this.button_salirDes = Resources.Load("images/Iconos/Salir_Des") as Texture;
		this.button_aceptarDes = Resources.Load("images/Iconos/Tildado_Des") as Texture;
		this.button_rechazarDes = Resources.Load("images/Iconos/Tachado_Des") as Texture;
		//----------------------------------------------------------------------------------
		this.sonido_come= Resources.Load("sonidos/comer_sonidosFX") as AudioClip;
		this.sonido_come2= Resources.Load("sonidos/comer2_sonidosFX") as AudioClip;
		this.cuadros= Resources.Load("images/blank") as Texture;
		
		this.players = new List<CPlayer>();
		for (byte i=0; i<4; ++i)
		{
			GameObject obj = new GameObject(string.Format("player{0}", i));
			CPlayer player = obj.AddComponent<CPlayer>();
			player.initialize(i);
			this.players.Add(player);
		}
		
		this.board = new List<short>();

		this.Font_nombre = Resources.Load ("fuentes/BABYBLOC") as Font;
		
		reset ();
	}
	
	//-------------------------------------------------------------------------------------
	void  Start (){
		InvokeRepeating("animacion",1,1);
		//-------------------------------------------------------
		//yE=80;
		xP=15;
		xP2=630;
		xP3=90;
		yP =350;
		yP2 =160;
		//--------------------------------------------------------
		xP=global2.ancho*(xP)/800;
		yP=global2.alto*(yP)/600;
		xP2=global2.ancho*(xP2)/800;
		xP3=global2.ancho*(xP3)/800;
		yP2 = global2.alto * (yP2)/600;
		//---------------------------------------------------------
		//---------------------------------------------------------
		//yE=80;
		TxP=40;
		TxP2=670;
		TyP =375;
		TyP2 =190;
		//--------------------------------------------------------
		TxP=global2.ancho*(TxP)/800;
		TyP=global2.alto*(TyP)/600;
		TyP2=global2.alto*(TyP2)/600;
		//yE=global2.ancho*yE/800;
		TxP2=global2.ancho*(TxP2)/800;
		//---------------------------------------------------------
		//---------------------------------------------------------
		yE=80;
		yE2 = 100;
		BxP=90;
		BxP2=575;
		BxP3=675;
		ByP =490;
		ByP2 =10;
		//--------------------------------------------------------
		BxP=global2.ancho*(BxP)/800;
		ByP=global2.alto*(ByP)/600;
		ByP2=global2.alto*(ByP2)/600;
		yE=global2.ancho*yE/800;
		yE2=global2.ancho*yE2/800;
		BxP2=global2.ancho*(BxP2)/800;
		BxP3=global2.ancho*(BxP3)/800;
		//---------------------------------------------------------

	}
	//-------------------------------------------------------------------------------------
	
	void reset()
	{
		player0=1;
		player1=1;
		player2=1;
		player3=1;
		checkplayer0= false;
		checkplayer1= false;
		checkplayer2= false;
		checkplayer3= false;
		
		this.players.ForEach (obj => obj.clear ());
		this.players [0].add (42);// 6  42  0 48
		this.players [1].add (0);
		
		this.players [2].add (6);
		this.players [3].add (48);
		//print ("enemigo");
		this.players [1].change_to_player2();;//turno enemigo
		this.players [2].change_to_player3();//turno enemigo
		this.players [3].change_to_AI1();//turno enemigo
		this.board.Clear ();
		this.table_board.Clear ();
		for (int i=0; i<COL_COUNT * COL_COUNT; ++i) {
			this.board.Add (short.MaxValue);
			this.table_board.Add ((short)i);
		}
		
		this.players.ForEach (obj => {
			obj.cell_indexes.ForEach (cell => {
				this.board [cell] = obj.player_index;
			});
		});
		
		this.current_player_index = 0;//seleccion
		this.step = 0;//turno jugador 1
	}
	
	float ratio = 1.0f;
	void OnGUI()
	{
		var Estilo_nombre = new GUIStyle();
		Estilo_nombre.font = Font_nombre;
		Estilo_nombre.normal.textColor = Color.white;
		//----------------------------------------------
		ratio = Screen.width / 800.0f;
		
		GUI.skin = this.blank_skin;
		draw_board();
		GUI.skin = this.blank_skin1;


		GUI.DrawTexture(new Rect(TxP,TyP,yE,yE),this.img_players [0]);
		GUI.Label (new Rect (xP,yP,yE2,yE2),"Billy: "+player0,Estilo_nombre);
		GUI.DrawTexture(new Rect(TxP,TyP2,yE,yE),this.img_players [1]);
		GUI.Label (new Rect (xP,yP2,yE2,yE2), "Sandy: " + player1,Estilo_nombre);
		GUI.DrawTexture(new Rect(TxP2,TyP2,yE,yE),this.img_players [2]);
		GUI.Label (new Rect (xP2,yP2,yE2,yE2),"Tony: "+player2,Estilo_nombre);
		GUI.DrawTexture(new Rect(TxP2,TyP,yE,yE),this.img_players [3]);
		GUI.Label (new Rect (xP2,yP,yE2,yE2), "Pilly: " + player3,Estilo_nombre);

		if (GUI.Button(new Rect(xP3,ByP2,yE, yE /* * ratio */), this.button_playagain))
		{
			StopAllCoroutines();
			AudioSource.PlayClipAtPoint(sonido_again,transform.position);
			reset();
		}
		CPlayer attacker_player = this.players[this.current_player_index];
		if (attacker_player.state != PLAYER_STATE.AI1) 
		{
			if (GUI.Button(new Rect(xP2,ByP2,yE, yE /* * ratio */), this.button_next))
			{
				StopAllCoroutines();
				//reset();
				//audio.PlayOneShot(sonido_next,1);
				AudioSource.PlayClipAtPoint(sonido_next,transform.position);
				saltar_turno();
			}
		}
		else
		{
			GUI.DrawTexture(new Rect(xP2,ByP2,yE, yE/* * ratio */), this.button_nextDes);
		}
		//----------------------------------------------------------------------------------
		if(apertura==true)
		{
			if (GUI.Button(new Rect(BxP,ByP,yE, yE /* * ratio */), this.button_salirDes))
			{
				apertura=false;
				//GUI.DrawTexture(new Rect(10,500,80 * ratio, 80 * ratio), this.button_salir);
				//StopAllCoroutines();
				//reset();
			}
		}
		else
		{
			GUI.DrawTexture(new Rect(BxP,ByP,yE+5, yE+5 /* * ratio */), this.button_salir);
		}
		if(apertura==false)
		{
			if (GUI.Button(new Rect(BxP2,ByP,yE, yE /* * ratio */), this.button_aceptarDes))
			{
				Application.LoadLevel("Menu");
			}
			if (GUI.Button(new Rect(BxP3,ByP,yE, yE /* * ratio */), this.button_rechazarDes))
			{
				apertura=true;
			}
		}
		//----------------------------------------------------------------------------------------
	}
	
	void draw_board()
	{
		float scaled_height = 400.0f * ratio;
		float gap_height = Screen.height - scaled_height;
		
		float outline_left = 0;
		float outline_top = gap_height * 0.5f;
		float outline_width = Screen.width;
		float outline_height = scaled_height;
		
		float hor_center = outline_width * 0.5f;
		float ver_center = outline_height * 0.5f;
		
		GUI.BeginGroup(new Rect(0, 0, outline_width, Screen.height));
		
		// Dibujar una tabla (alineación: centro).
		//GUI.DrawTexture(new Rect(0, outline_top, outline_width, outline_height), this.game_board);
		
		int width = (int)(50 * ratio);
		int celloutline_width = width * CBattleRoom.COL_COUNT;
		float half_celloutline_width = celloutline_width*0.5f;
		
		GUI.BeginGroup(new Rect(hor_center-half_celloutline_width, 
		                        ver_center-half_celloutline_width + outline_top+20, celloutline_width, celloutline_width));
		
		List<int> current_turn = new List<int>();
		short index = 0;
		for (int row=0; row<CBattleRoom.COL_COUNT; ++row)
		{
			int gap_y = 0;//(row * 1);
			for (int col=0; col<CBattleRoom.COL_COUNT; ++col)
			{
				int gap_x = 0;//(col * 1);
				
				Rect cell_rect = new Rect(col * width + gap_x, row * width + gap_y, width, width);
				if (GUI.Button(cell_rect, this.cuadros))
				{
					on_click(index);//controles para las celdas
				}
				
				if (this.board[index] != short.MaxValue)
				{
					int player_index = this.board[index];
					GUI.DrawTexture(cell_rect, this.img_players[player_index]);
					
					if (this.current_player_index == player_index)
					{
						GUI.DrawTexture(cell_rect, this.focus_cell);
					}
				}
				
				if (this.available_attack_cells.Contains(index))
				{
					GUI.DrawTexture(cell_rect, this.focus_cell);
				}
				
				++index;
			}
		}
		GUI.EndGroup();
		GUI.EndGroup();
	}
	
	short selected_cell = short.MaxValue;
	void on_click(short cell)
	{
		//Debug.Log(cell);
		CPlayer attacker_player = this.players[this.current_player_index];
		print(attacker_player.state);
		print (this.step);
		switch(this.step)
		{
		case 0:

			if (attacker_player.state != PLAYER_STATE.AI1)
			{
				if (validate_begin_cell(cell))
				{
					this.selected_cell = cell;
					//Debug.Log("go to step2");
					this.step = 1;//jugador 2
					//print(this.step);
					refresh_available_cells(this.selected_cell);
				}
				else {
					print ("no hay mas movimientos");
					this.step = 0;
					refresh_available_cells(this.selected_cell);
				}
			}
			break;
			
		case 1:
		{
			if (attacker_player.state != PLAYER_STATE.AI1)
			{
				// Cuando tocaste tu celular nuevo.
				if (this.players[this.current_player_index].cell_indexes.Exists(obj => obj == cell))
				{
					this.selected_cell = cell;
					refresh_available_cells(this.selected_cell);
					break;
				}
				// No se puede tocar la celda de otro jugador.
				foreach(CPlayer player in this.players)
				{
					if (player.cell_indexes.Exists(obj => obj == cell))
					{
						return;
					}
				}
				
				this.step = 2;//jugador 3
				//print(this.step);
				StartCoroutine(on_selected_cell_to_attack(cell));
			}
		}
			break;
			
		case 2:
		{
			// Playin AI now.
			//print ("cambio de turno");
			if(this.current_player_index ==0)
			{
				player0=0;
			}
			if(this.current_player_index ==1)
			{
				player1=0;
			}
			if(this.current_player_index ==2)
			{
				player2=0;
			}
			if(this.current_player_index ==3)
			{
				player3=0;
			}
			print ("CAMBIO DE TURNO");
			phase_end();
			break;
		}
			/*case 3:
		// Playin AI now.
		print ("cambio de turno");
		break;*/
		}
	}
	
	IEnumerator on_selected_cell_to_attack(short cell)
	{
		byte distance = CHelper.howfar_from_clicked_cell(this.selected_cell, cell);
		if (distance == 1)
		{
			// copiar a la celda
			if(this.current_player_index==0)
			{
				player0= player0+1;
			}
			if(this.current_player_index==1)
			{
				player1= player1+1;
			}
			if(this.current_player_index==2)
			{
				player2= player2+1;
			}
			if(this.current_player_index==3)
			{
				player3= player3+1;
			}
			disponibles=disponibles-1;
			//print ("disponibles:"+disponibles);
			//print ("Zorro:"+player0);
			//print ("Gato:"+player1);
			//print ("Rojo:"+player2);
			//print ("Azul:"+player3);
			yield return StartCoroutine(reproduce(cell));
			//print ("TE TOCA A VOS");
			phase_end();
		}
		else if (distance == 2)
		{
			// mover
			this.board[this.selected_cell] = short.MaxValue;
			this.players[this.current_player_index].remove(this.selected_cell);
			yield return StartCoroutine(reproduce(cell));
			print ("YA MOVI");
			phase_end();
		}
		
		yield return 0;
	}
	
	void game_over()
	{
		Debug.Log("GameOver!");
		if (player0 <= 0 && checkplayer0==false) 
		{
			/*if(player1 > 0 && (player2 > 0 || player3 > 0))
			{*/
			this.players[0].clear();
			checkplayer0=true;
			/*}
			else
			{
				if(player2 > 0 && (player1 > 0 || player3 > 0))
				{
					this.players[0].clear();
					checkplayer0=true;
				}
			}*/
		}
		if (player1 <= 0 && checkplayer1==false) 
		{
			/*if(player0 > 0 && (player2 > 0 || player3 > 0))
			{*/
			this.players[1].clear();
			checkplayer1=true;
			/*}
			else
			{
				if(player2 > 0 && (player0 > 0 || player3 > 0))
				{
					this.players[1].clear();
					checkplayer1=true;
				}
			}*/
		}
		if (player2 <= 0 && checkplayer2==false) 
		{
			/*if(player1 > 0 && (player0 > 0 || player3 > 0))
			{*/
			this.players[2].clear();
			checkplayer2=true;
			/*}
			else
			{
				if(player0 > 0 && (player1 > 0 || player3 > 0))
				{
					this.players[2].clear();
					checkplayer2=true;	
				}
			}*/
		}
		if (player3 <= 0 && checkplayer3==false) 
		{
			/*if(player1 > 0 && (player2 > 0 || player0 > 0))
			{*/
			this.players[3].clear();
			checkplayer3=true;
			/*}
			else
			{
				if(player2 > 0 && (player1 > 0 || player0 > 0))
				{
					this.players[3].clear();
					checkplayer3=true;
				}
			}*/
		}
		//-----------------------------------------------------------------------
		print (checkplayer0);
		print (checkplayer1);
		print (checkplayer2);
		print (checkplayer3);
		if (checkplayer0 == true) 
		{
			if (checkplayer1 == true && (checkplayer2 == true || checkplayer3 == true)) {
				win ();
			}
			if(checkplayer2==true && (checkplayer1==true || checkplayer3== true))		
			{
				win();
			}
		} 
		else 
		{
			if (checkplayer1 == true) 
			{
				if(checkplayer0==true && (checkplayer2==true || checkplayer3== true))		
				{
					win();
				}
				if(checkplayer2==true && (checkplayer0==true || checkplayer3== true))		
				{
					win();
				}
			}
			
			else
			{
				if (checkplayer2 == true) 
				{
					if(checkplayer1==true && (checkplayer0==true || checkplayer3== true))		
					{
						win();
					}
					if(checkplayer0==true && (checkplayer1==true || checkplayer3== true))		
					{
						win();
					}
				}
				
				else
				{
					if (checkplayer3 == true) 
					{
						if(checkplayer1==true && (checkplayer2==true || checkplayer0== true))		
						{
							win();
						}
						if(checkplayer2==true && (checkplayer0==true || checkplayer1== true))		
						{
							win();
						}
					}
				}
			}
		}
		if (disponibles <= 0) 
		{
			win();
		}
		//--------------------------------------------------------------------------
	}
	void win()
	{
		//----------------------------------------------------------------
		if (player0 > player1 && player0 > player2 && player0 > player3) 
		{
			Ganar.gano=0;
			print ("ganador:"+player0);
			print ("otros:"+player1);
			print ("otros:"+player2);
			print ("otros:"+player3);
		}
		if (player1 > player0 && player1 > player2 && player1 > player3) 
		{
			Ganar.gano=1;	
			print ("ganador:"+player1);
			print ("otros:"+player0);
			print ("otros:"+player2);
			print ("otros:"+player3);
			
		}
		if (player2 > player1 && player2 > player0 && player2 > player3) 
		{
			Ganar.gano=2;	
			print ("ganador:"+player2);
			print ("otros:"+player1);
			print ("otros:"+player0);
			print ("otros:"+player3);
		}
		if (player3 > player1 && player3 > player2 && player3 > player0) 
		{
			Ganar.gano=3;	
			print ("ganador:"+player3);
			print ("otros:"+player1);
			print ("otros:"+player2);
			print ("otros:"+player0);
		}
		//------------------------------------------------------------------
		Application.LoadLevel ("Perder");
	}
	void phase_end()
	{
		CPlayer victim_player = this.players[this.current_player_index];
		
		//cambio de jugador
		if (this.current_player_index == 0)
		{
			if(checkplayer1==false)
			{
				this.current_player_index = 1;
			}
			else
			{
				if(checkplayer2==false)
				{
					this.current_player_index = 2;
				}
				else
				{
					this.current_player_index = 3;
				}
			}
			
		}
		else
		{
			if (this.current_player_index == 1)
			{
				if(checkplayer2==false)
				{
					this.current_player_index = 2;
				}
				else
				{
					if(checkplayer3==false)
					{
						this.current_player_index = 3;
					}
					else
					{
						this.current_player_index = 0;
					}
				}
				
			}
			else
			{
				if (this.current_player_index == 2)
				{
					if(checkplayer3==false)
					{
						this.current_player_index = 3;
					}
					else
					{
						if(checkplayer0==false)
						{
							this.current_player_index = 0;
						}
						else
						{
							this.current_player_index = 1;
						}
					}
					
				}
				else
				{
					if (this.current_player_index == 3)
					{
						if(checkplayer0==false)
						{
							this.current_player_index = 0;
						}
						else
						{
							if(checkplayer1==false)
							{
								this.current_player_index = 1;
							}
							else
							{
								this.current_player_index = 2;
							}
						}
						
					}
				}
			}
			
			//this.current_player_index = 0;
		}
		
		
		if (!CHelper.can_play_more(this.table_board, this.players, this.current_player_index) || disponibles<=0)
		{
			print (disponibles);
			game_over();
			return;
		}
		
		CPlayer attacker_player = this.players[this.current_player_index];
		if (attacker_player.state == PLAYER_STATE.AI1)
		{
			this.step = 1;
			//this.step = 2;
			//print(this.step);
			StartCoroutine(play_agent(attacker_player, victim_player));
		}
		else
		{
			this.step = 0;
			//this.step = 0;
			//print(this.step);
		}
		this.available_attack_cells.Clear();
	}
	
	IEnumerator play_agent(CPlayer attacker_player, CPlayer victim_player)
	{
		CellInfo choice = attacker_player.run_agent(this.table_board, this.players, victim_player.cell_indexes);
		yield return new WaitForSeconds(0.5f);
		
		//Debug.Log(string.Format("{0} -> {1} = {2}", choice.from_cell, choice.to_cell, choice.score)); HABILITAR
		this.selected_cell = choice.from_cell;
		StartCoroutine(on_selected_cell_to_attack(choice.to_cell));
	}
	
	void refresh_available_cells(short cell)
	{
		this.available_attack_cells = CHelper.find_available_cells(cell, this.table_board, this.players);
	}
	
	void clear_available_attacking_cells()
	{
		this.available_attack_cells.Clear();
	}
	
	//IEnumerator moving()
	//{
	//}
	
	IEnumerator reproduce(short cell)
	{
		CPlayer current_player = this.players[this.current_player_index];
		//CPlayer other_player = this.players.Find(obj => obj.player_index != this.current_player_index);
		CPlayer other_player1= this.players.Find(obj => obj.player_index != this.current_player_index);
		CPlayer other_player2= this.players.Find(obj => obj.player_index != this.current_player_index && other_player1.player_index != obj.player_index );
		CPlayer other_player3= this.players.Find(obj => obj.player_index != this.current_player_index && other_player1.player_index != obj.player_index && other_player2.player_index != obj.player_index);
		//print ("soy comible:"+other_player1);	
		//print ("soy comible:"+other_player2);
		//print ("soy comible:"+other_player3);
		clear_available_attacking_cells();
		yield return new WaitForSeconds(0.5f);
		
		this.board[cell] = current_player.player_index;
		current_player.add(cell);
		
		yield return new WaitForSeconds(0.5f);
		
		// comer
		//--------------------------------------------------------------------------------------------------------------
		List<short> neighbors1 = CHelper.find_neighbor_cells(cell, other_player1.cell_indexes, 1/*Espacio para comer*/);
		foreach (short obj in neighbors1)
		{
			//---------------------------------------------------
			if(this.current_player_index==0)
			{
				//print ("zorro vamosss");
				GetComponent<AudioSource>().PlayOneShot(sonido_come,1);
				player0= player0+1;
			}
			if(this.current_player_index==1)
			{
				//print ("gato vamosss");
				GetComponent<AudioSource>().PlayOneShot(sonido_come2,1);
				player1= player1+1;
			}
			if(this.current_player_index==2)
			{
				//print ("rojo vamosss");
				GetComponent<AudioSource>().PlayOneShot(sonido_come,1);
				player2= player2+1;
			}
			if(this.current_player_index==3)
			{
				//print ("azul vamosss");
				GetComponent<AudioSource>().PlayOneShot(sonido_come2,1);
				player3= player3+1;
			}
			//-----------------------------------------
			if(other_player1.player_index==0)
			{
				//print ("zorro muere");
				this.img_players [0] = (Resources.Load ("images/Personajes/Perro/Perro_01") as Texture);
				player0= player0-1;
			}
			if(other_player1.player_index==1)
			{
				//print ("gato muere");
				this.img_players [1] = (Resources.Load ("images/Personajes/Gato/Gato_01") as Texture);
				player1= player1-1;
			}
			if(other_player1.player_index==2)
			{
				//print ("rojo muere");
				this.img_players [2] = (Resources.Load ("images/Personajes/Raton/Raton_01") as Texture);
				player2= player2-1;
			}
			if(other_player1.player_index==3)
			{
				//print ("azul muere");
				this.img_players [3] = (Resources.Load ("images/Personajes/Cerdo/Cerdo_01") as Texture);
				player3= player3-1;
			}
			//-------------------------------------------------------
			this.board[obj] = current_player.player_index;
			current_player.add(obj);
			
			other_player1.remove(obj);
			//print ("soy:"+current_player);
			//print ("me comi a:"+other_player1);
			//phase_end();
			yield return new WaitForSeconds(0.2f);
		}
		//-------------------------------------------------------------------------------------------------------------
		//--------------------------------------------------------------------------------------------------------------
		List<short> neighbors2 = CHelper.find_neighbor_cells(cell, other_player2.cell_indexes, 1/*Espacio para comer*/);
		foreach (short obj in neighbors2)
		{
			//----------------------------------------------
			if(this.current_player_index==0)
			{
				//print ("zorro vamosss");
				GetComponent<AudioSource>().PlayOneShot(sonido_come,1);
				player0= player0+1;
			}
			if(this.current_player_index==1)
			{
				//print ("gato vamosss");
				GetComponent<AudioSource>().PlayOneShot(sonido_come2,1);
				player1= player1+1;
			}
			if(this.current_player_index==2)
			{
				//print ("rojo vamosss");
				GetComponent<AudioSource>().PlayOneShot(sonido_come,1);
				player2= player2+1;
			}
			if(this.current_player_index==3)
			{
				//print ("azul vamosss");
				GetComponent<AudioSource>().PlayOneShot(sonido_come2,1);
				player3= player3+1;
			}
			if(other_player2.player_index==0)
			{
				//print ("zorro muere");
				this.img_players [0] = (Resources.Load ("images/Personajes/Perro/Perro_01") as Texture);
				player0= player0-1;
			}
			if(other_player2.player_index==1)
			{
				//print ("gato muere");
				this.img_players [1] = (Resources.Load ("images/Personajes/Gato/Gato_01") as Texture);
				player1= player1-1;
			}
			if(other_player2.player_index==2)
			{
				//print ("rojo muere");
				this.img_players [2] = (Resources.Load ("images/Personajes/Raton/Raton_01") as Texture);
				player2= player2-1;
			}
			if(other_player2.player_index==3)
			{
				//print ("azul muere");
				this.img_players [3] = (Resources.Load ("images/Personajes/Cerdo/Cerdo_01") as Texture);
				player3= player3-1;
			}
			//----------------------------------------------
			this.board[obj] = current_player.player_index;
			current_player.add(obj);
			
			other_player2.remove(obj);
			//print ("soy:"+current_player);
			//print ("me comi a:"+other_player2);
			yield return new WaitForSeconds(0.2f);
		}
		//-------------------------------------------------------------------------------------------------------------
		//--------------------------------------------------------------------------------------------------------------
		List<short> neighbors3 = CHelper.find_neighbor_cells(cell, other_player3.cell_indexes, 1/*Espacio para comer*/);
		foreach (short obj in neighbors3)
		{
			//-------------------------------------------------
			if(this.current_player_index==0)
			{
				//print ("zorro vamosss");
				GetComponent<AudioSource>().PlayOneShot(sonido_come,1);
				player0= player0+1;
			}
			if(this.current_player_index==1)
			{
				//print ("gato vamosss");
				GetComponent<AudioSource>().PlayOneShot(sonido_come2,1);
				player1= player1+1;
			}
			if(this.current_player_index==2)
			{
				//print ("rojo vamosss");
				GetComponent<AudioSource>().PlayOneShot(sonido_come,1);
				player2= player2+1;
			}
			if(this.current_player_index==3)
			{
				//print ("azul vamosss");
				GetComponent<AudioSource>().PlayOneShot(sonido_come2,1);
				player3= player3+1;
			}
			if(other_player3.player_index==0)
			{
				//print ("zorro muere");
				this.img_players [0] = (Resources.Load ("images/Personajes/Perro/Perro_01") as Texture);
				player0= player0-1;
			}
			if(other_player3.player_index==1)
			{
				//print ("gato muere");
				this.img_players [1] = (Resources.Load ("images/Personajes/Gato/Gato_01") as Texture);
				player1= player1-1;
			}
			if(other_player3.player_index==2)
			{
				//print ("rojo muere");
				this.img_players [2] = (Resources.Load ("images/Personajes/Raton/Raton_01") as Texture);
				player2= player2-1;
			}
			if(other_player3.player_index==3)
			{
				//print ("azul muere");
				this.img_players [3] = (Resources.Load ("images/Personajes/Cerdo/Cerdo_01") as Texture);
				player3= player3-1;
			}
			//-------------------------------------------------
			this.board[obj] = current_player.player_index;
			current_player.add(obj);
			
			other_player3.remove(obj);
			//print ("soy:"+current_player);
			//print ("me comi a:"+other_player3);
			yield return new WaitForSeconds(0.2f);
		}
		//-------------------------------------------------------------------------------------------------------------
	}
	
	bool validate_begin_cell(short cell)
	{
		return this.players[this.current_player_index].cell_indexes.Exists(obj => obj == cell);
	}
	
	void  animacion (){
		//print("hola");
		if (contador < 2) 
		{
			contador++;
		}
		if (contador == 1)
		{
			if(player0==0)
				{
				this.img_players [0] = (Resources.Load ("images/Personajes/Perro/Perro_Muerto") as Texture);
				}
			else
				{
				this.img_players [0] = (Resources.Load ("images/Personajes/Perro/Perro_02") as Texture);
				}
			if(player1==0)
				{
				this.img_players [1] = (Resources.Load ("images/Personajes/Gato/Gato_Muerto") as Texture);
				}
			else
				{
				this.img_players [1] = (Resources.Load ("images/Personajes/Gato/Gato_02") as Texture);
				}
			if(player2==0)
				{
				this.img_players [2] = (Resources.Load ("images/Personajes/Raton/Raton_Muerto") as Texture);
				}
			else 
				{
				this.img_players [2] = (Resources.Load ("images/Personajes/Raton/Raton_02") as Texture);
				}
			if(player3==0)
				{
				this.img_players [3] = (Resources.Load ("images/Personajes/Cerdo/Cerdo_Muerto") as Texture);
				}
			else
				{
				this.img_players [3] = (Resources.Load ("images/Personajes/Cerdo/Cerdo_02") as Texture);
				}
		}
		if (contador == 2) 
		{
			if(player0==0)
			{
				this.img_players [0] = (Resources.Load ("images/Personajes/Perro/Perro_Muerto") as Texture);
			}
			else
			{
				this.img_players [0] = (Resources.Load ("images/Personajes/Perro/Perro_03") as Texture);
			}
			if(player1==0)
			{
				this.img_players [1] = (Resources.Load ("images/Personajes/Gato/Gato_Muerto") as Texture);
			}
			else
			{
				this.img_players [1] = (Resources.Load ("images/Personajes/Gato/Gato_03") as Texture);
			}
			if(player2==0)
			{
				this.img_players [2] = (Resources.Load ("images/Personajes/Raton/Raton_Muerto") as Texture);
			}
			else 
			{
				this.img_players [2] = (Resources.Load ("images/Personajes/Raton/Raton_03") as Texture);
			}
			if(player3==0)
			{
				this.img_players [3] = (Resources.Load ("images/Personajes/Cerdo/Cerdo_Muerto") as Texture);
			}
			else
			{
				this.img_players [3] = (Resources.Load ("images/Personajes/Cerdo/Cerdo_03") as Texture);
			}
		}
		if (contador == 2) 
		{
			contador=0;
		}
	}
	void saltar_turno(){
	phase_end ();
	}
}
