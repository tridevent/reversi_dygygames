﻿using UnityEngine;
using System.Collections;

public class Ganar : MonoBehaviour {
	public static byte gano;//gano 0 zorro, gano 1 gato, gano 2 rojo, gano 3 azul
	public GUITexture cartel;
	int contador;
	// Use this for initialization
	void Start(){
		contador = 0;
		//-----------------------------------------------------------------------------------------
		if (gano == 0) 
		{
			cartel.texture=(Resources.Load ("images/Personajes/Perro/Perro_02") as Texture);
			print("El ganador es el perro");
		}
		if (gano == 1) 
		{
			cartel.texture=(Resources.Load ("images/Personajes/Gato/Gato_02") as Texture);
			print("El ganador es el gato");
		}
		if (gano == 2) 
		{
			cartel.texture=(Resources.Load ("images/Personajes/Raton/Raton_02") as Texture);
			print("El ganador es el raton");
		}
		if (gano == 3) 
		{
			cartel.texture=(Resources.Load ("images/Personajes/Cerdo/Cerdo_02") as Texture);
			print("El ganador es el cerdo");
		}
		//--------------------------------------------------------------------------------------------
		InvokeRepeating ("animacion", 1, 1);
	}
	void animacion () {
	if (contador < 2) 
		{
			contador++;
		}
	if(contador==1)
		{
		if (gano == 0) 
			{
				cartel.texture=(Resources.Load ("images/Personajes/Perro/Perro_02") as Texture);
				print("El ganador es el perro");
			}
		if (gano == 1) 
			{
				cartel.texture=(Resources.Load ("images/Personajes/Gato/Gato_02") as Texture);
				print("El ganador es el gato");
			}
		if (gano == 2) 
			{
				cartel.texture=(Resources.Load ("images/Personajes/Raton/Raton_02") as Texture);
				print("El ganador es el raton");
			}
		if (gano == 3) 
			{
				cartel.texture=(Resources.Load ("images/Personajes/Cerdo/Cerdo_02") as Texture);
				print("El ganador es el cerdo");
			}
		}
	if(contador==2)
		{
		if (gano == 0) 
			{
				cartel.texture=(Resources.Load ("images/Personajes/Perro/Perro_03") as Texture);
				print("El ganador es el perro");
			}
		if (gano == 1) 
			{
				cartel.texture=(Resources.Load ("images/Personajes/Gato/Gato_03") as Texture);
				print("El ganador es el gato");
			}
		if (gano == 2) 
			{
				cartel.texture=(Resources.Load ("images/Personajes/Raton/Raton_03") as Texture);
				print("El ganador es el raton");
			}
		if (gano == 3) 
			{
				cartel.texture=(Resources.Load ("images/Personajes/Cerdo/Cerdo_03") as Texture);
				print("El ganador es el cerdo");
			}
		}
	if (contador == 2) 
		{
			contador=0;
		}
	}

}
