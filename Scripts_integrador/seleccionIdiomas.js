﻿static var idiomaActual:int=0; //0 ingles 1 español
var ingles:GameObject;
var espanol:GameObject;
//--------------------------------------------------------
var inglesActivado:Texture;
var espanolActivado:Texture;
private var BotonPulsado : boolean = false;
var nivel:String;
//--------------------ingles----------------------------------
private var yE:float=0;
private var xP:float=0;
private var yP:float=0;
//----------------español--------------------------------------
private var yE2:float=0;
private var xP2:float=0;
private var yP2:float=0;
//------------------------------------------------------

function Start () {
//-------------------------------------------------------
yE=ingles.GetComponent.<GUITexture>().pixelInset.width;
xP=ingles.GetComponent.<GUITexture>().pixelInset.x;
yP=ingles.GetComponent.<GUITexture>().pixelInset.y;
//--------------------------------------------------------
yE2=espanol.GetComponent.<GUITexture>().pixelInset.width;
xP2=espanol.GetComponent.<GUITexture>().pixelInset.x;
yP2=espanol.GetComponent.<GUITexture>().pixelInset.y;
//--------------------------------------------------------
xP=global.ancho*(xP)/800;
yP=global.alto*(yP)/600;
yE=global.ancho*yE/800;
//--------------------------------------------------------
xP2=global.ancho*(xP2)/800;
yP2=global.alto*(yP2)/600;
yE2=global.ancho*yE2/800;
//------------------------------------------------------------
ingles.GetComponent.<GUITexture>().pixelInset = Rect(xP,yP,yE,yE);
espanol.GetComponent.<GUITexture>().pixelInset = Rect(xP2,yP2,yE2,yE2);
}

function Update () {

	if(Input.touchCount > 0)
	    {
	        for (var i = 0; i < Input.touchCount; ++i) 
	        {   
	            if(ingles.GetComponent.<GUITexture>().HitTest(Input.GetTouch(i).position))
	            {        
	                //BotonPulsado = true;
	                if(fondo_DobleTouch.temporizador>0)
		    		{
		    		idiomaActual=0;
	                ActivarIngles();
	                }                          	           
	            }
	            if(espanol.GetComponent.<GUITexture>().HitTest(Input.GetTouch(i).position))
	            {        
	                //BotonPulsado = true;
	                if(fondo_DobleTouch.temporizador>0)
		    		{
		    		idiomaActual=1;
	                ActivarEspanol();
	                }                          	           
	            }
	        } 
	    } 
		if (Input.GetMouseButton(0))
		{
	    	if (ingles.GetComponent.<GUITexture>().HitTest(Input.mousePosition))
	    	{
	    	//BotonPulsado = true;
	    	if(fondo_DobleTouch.temporizador>0)
		    		{
		    		idiomaActual=0;
	                ActivarIngles();
	                } 
	    	}
	    	if (espanol.GetComponent.<GUITexture>().HitTest(Input.mousePosition))
	    	{
	    	//BotonPulsado = true;
	    	if(fondo_DobleTouch.temporizador>0)
		    		{
		    		idiomaActual=1;
	                ActivarEspanol();
	                } 
	    	}
	    }
	    if(BotonPulsado)
	    {
		Application.LoadLevel(nivel);	
	    }          		    	
	    BotonPulsado = false;
	
}
function ActivarIngles()
{
ingles.GetComponent.<GUITexture>().texture= inglesActivado;
yield WaitForSeconds (0.3);
BotonPulsado = true;
}
function ActivarEspanol()
{
espanol.GetComponent.<GUITexture>().texture= espanolActivado;
yield WaitForSeconds (0.3);
BotonPulsado = true;
}




